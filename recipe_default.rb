# plugins management
pluginfiles = node['bbt_sonarqube']['plugins']
pluginfiles.each do |file, source|
  remote_file "/opt/sonar/sonarqube-5.6.6/extensions/plugins/#{file}" do
    owner 'sonar'
      group 'sonar'
      mode 0744
      source source
      use_etag true
      headers('Authorization' => "#{node['artifactory']['authorization']}")
      use_conditional_get true
      action :create_if_missing
  end
end
