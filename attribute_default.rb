default['bbt_sonarqube']['plugins'] = {
    "sonar-widget-lab-plugin-1.8.1.jar": 'https://artifactory.bbtnet.com/artifactory/maven-mirror/org/sonarsource/widget-lab/sonar-widget-lab-plugin/1.8.1/sonar-widget-lab-plugin-1.8.1.jar',
    "sonar-ldap-plugin-2.0.jar": 'https://artifactory.bbtnet.com/artifactory/maven-mirror/org/sonarsource/ldap/sonar-ldap-plugin/2.0/sonar-ldap-plugin-2.0.jar',
    "sonar-javascript-plugin-2.21.1.4786.jar": 'https://artifactory.bbtnet.com/artifactory/maven-mirror/org/sonarsource/javascript/sonar-javascript-plugin/2.21.1.4786/sonar-javascript-plugin-2.21.1.4786.jar',
    "sonar-clover-plugin-3.1.jar": 'https://artifactory.bbtnet.com/artifactory/maven-mirror/org/sonarsource/clover/sonar-clover-plugin/3.1/sonar-clover-plugin-3.1.jar',
    "sonar-checkstyle-plugin-2.4.jar": 'https://artifactory.bbtnet.com/artifactory/maven-mirror/org/sonarsource/checkstyle/sonar-checkstyle-plugin/2.4/sonar-checkstyle-plugin-2.4.jar',
    "sonar-timeline-plugin-1.5.jar": 'https://artifactory.bbtnet.com/artifactory/maven-mirror/org/codehaus/sonar-plugins/sonar-timeline-plugin/1.5/sonar-timeline-plugin-1.5.jar',
    "sonar-tab-metrics-plugin-1.4.1.jar": 'https://artifactory.bbtnet.com/artifactory/maven-mirror/org/codehaus/sonar-plugins/sonar-tab-metrics-plugin/1.4.1/sonar-tab-metrics-plugin-1.4.1.jar',
    "sonar-clirr-plugin-1.3.jar": 'https://artifactory.bbtnet.com/artifactory/maven-mirror/org/sonarsource/clirr/sonar-clirr-plugin/1.3/sonar-clirr-plugin-1.3.jar',
    "sonar-xml-plugin-1.3.jar": 'https://artifactory.bbtnet.com/artifactory/maven-mirror/org/sonarsource/sonar-xml-plugin/sonar-xml-plugin/1.3/sonar-xml-plugin-1.3.jar',
    "sonar-java-plugin-4.10.0.10260.jar": 'https://artifactory.bbtnet.com/artifactory/maven-mirror/org/sonarsource/java/sonar-java-plugin/4.10.0.10260/sonar-java-plugin-4.10.0.10260.jar',
    "sonar-cfamily-plugin-4.10.0.8366.jar": 'https://artifactory.bbtnet.com/artifactory/jenkins-tools/sonar-cfamily-plugin-4.10.0.8366.jar',
    "sonar-cobol-plugin-3.4.0.1932.jar": 'https://artifactory.bbtnet.com/artifactory/jenkins-tools/sonar-cobol-plugin-3.4.0.1932.jar'
}
